# AlpgrowRpc
Is used as a common ground for comunicating between machenes. 

(Protobuf) -> [AlpgrowRpc] 

# Installation
## Install Protobuf 
<!-- this library is used to compile messages -->
mix escript.install hex protobuf


Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/alpgrow_rpc>.


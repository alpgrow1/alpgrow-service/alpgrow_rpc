defmodule Alpgrow.RPC.MixProject do
  use Mix.Project

  def project do
    [
      app: :alpgrow_rpc,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :exprotobuf, :amqp]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [

      {:swoosh, "~> 1.6"},
      {:exprotobuf, "~> 1.2"},
      {:amqp, "~> 3.1"},
      {:hackney, "~> 1.18"},
      {:skogsra, "~> 2.3"},

      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end

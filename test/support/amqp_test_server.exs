defmodule Alpgrow.RPC.AMQPTestServer do
  @behaviour Alpgrow.RPC.Handler
  alias Alpgrow.RPC.AMQP.Server
  @queue_name "test_server_rpc"

  def start_link do
    Server.start_link(queue: @queue_name, prefix: "", handler: __MODULE__)
  end

  def handle_rpc("cast:" <> payload) do
    {value, _bindings} = Code.eval_string(payload)
    IO.puts("#{value}")
    :ok
  end

  def handle_rpc("call:invalid") do
    {:error, "custom error reason"}
  end

  def handle_rpc("call:" <> payload) do
    {value, _bindings} = Code.eval_string(payload)
    {:ok, to_string(value)}
  end
end

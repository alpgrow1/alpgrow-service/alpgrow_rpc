defmodule Alpgrow.RPC.AMQPTestClient do
  alias Alpgrow.RPC.AMQP.Client
  @queue_name "server_rpc"

  def start_link do
    Client.start_link [prefix: "test_"]
  end

  def eval_code_call(str_code) do
    Client.rpc_call("call:" <> str_code, @queue_name)
  end

  def eval_code_cast(str_code) do
    Client.rpc_cast("cast:" <> str_code, @queue_name)
  end
end

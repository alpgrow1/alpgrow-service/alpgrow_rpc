ExUnit.start()

files = Path.wildcard "./test/support/*"

Enum.each files, fn e ->
  Code.require_file e
end

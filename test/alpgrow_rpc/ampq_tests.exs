defmodule Alpgrow.RPCTest do
  use ExUnit.Case
  alias Alpgrow.RPC.Config

  @test_queue "test_server_rpc"

  setup_all do
    amqp_opts = Config.amqp_options!
    {:ok, conn} = AMQP.Connection.open
    {:ok, chan} = AMQP.Channel.open conn

    AMQP.Queue.declare(chan, @test_queue)

    Alpgrow.RPC.AMQPTestClient.start_link
    Alpgrow.RPC.AMQPTestServer.start_link
    :ok
  end

  test "call evaluation" do
    result = Alpgrow.RPC.AMQPTestClient.eval_code_call("1 + 1")
    assert result == {:ok, "2"}
  end

  test "custom call exeption" do
    result = Alpgrow.RPC.AMQPTestClient.eval_code_call("1 + 1qwe")
    assert result == {:error, "exception"}
  end

  test "cast evaluation" do
    result = Alpgrow.RPC.AMQPTestClient.eval_code_cast(":ok")
    assert result == :ok
  end
end

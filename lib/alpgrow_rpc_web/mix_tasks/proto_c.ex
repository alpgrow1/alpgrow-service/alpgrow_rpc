defmodule Mix.Tasks.ProtoC do
  @moduledoc "The hello mix task: `mix help hello`"
  use Mix.Task


  @shortdoc "Simply calls the Hello.say/0 function."
  def run(_) do
    Path.wildcard("./lib/alpgrow_rpc/proto/*/*.proto")
    |> Enum.map(fn a -> run_protoc(a) end)
    |> IO.inspect()

    :ok
  end

  def run_protoc(a) when is_binary(a) do
    # ./lib/alpgrow_rpc/proto/{app_name}/{message}.proto
    app_name = Enum.at(String.split(a, "/"), 3)
    message = Enum.at String.split(Enum.at(String.split(a, "/"), 4), "."), 0

    protoPath = "./lib/alpgrow_rpc/messages/" <> app_name <> "/"

    with true <- File.exists?(protoPath) do
      # Compiles Proto3 messages
      System.cmd("protoc", ["--elixir_out=#{protoPath}/", a])
      # Stupid output subdirs
      # lib/alpgrow_rpc/messages/alpgrow_appengine_api/lib/alpgrow_rpc/proto/alpgrow_appengine_api/generic_error_reply.pb.ex

      File.rename(
        "lib/alpgrow_rpc/messages/#{app_name}/lib/alpgrow_rpc/proto/#{app_name}/#{message}.pb.ex",
        "lib/alpgrow_rpc/messages/#{app_name}/#{message}.pb.ex"
      )
      File.rm_rf  ( "lib/alpgrow_rpc/messages/#{app_name}/lib/")
      IO.puts "😎 Compiled: #{app_name} \t #{message}"

    else
      _ ->
        File.mkdir(protoPath)
        # Call again and compile protobuffers
        run_protoc(a)
    end
  end
end

defmodule Alpgrow.RPC.Protocol.EventHub do
  @external_resource Path.expand("./proto/event_hub", __DIR__)
  use Protobuf, from: Path.wildcard Path.expand("./proto/event_hub/*", __DIR__)

  use Alpgrow.RPC.Protocol, amqp_queue: "event_hub"
end

defmodule Alpgrow.RPC.Protocol.Backend do
  @external_resource Path.expand("./proto/backed", __DIR__)
  use Protobuf, from: Path.wildcard Path.expand("./proto/backend/*", __DIR__)

  use Alpgrow.RPC.Protocol, amqp_queue: "backend_rpc"
end

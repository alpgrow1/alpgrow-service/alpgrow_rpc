defmodule Alpgrow.RPC.Protocol.IotHub do
  @external_resource Path.expand("./proto/iot_hub", __DIR__)
  use Protobuf, from: Path.wildcard Path.expand("./proto/iot_hub/*", __DIR__)

  use Alpgrow.RPC.Protocol, amqp_queue: "iot_hub"
end

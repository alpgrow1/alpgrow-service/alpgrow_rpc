defmodule Alpgrow.RPC.Protocol.TaskHub do
  @external_resource Path.expand("./proto/task_hub", __DIR__)
  use Protobuf, from: Path.wildcard Path.expand("./proto/task_hub/*", __DIR__)

  use Alpgrow.RPC.Protocol, amqp_queue: "task_hub"
end

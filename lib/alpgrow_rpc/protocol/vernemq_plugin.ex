defmodule Alpgrow.RPC.Protocol.VerneMQPlugin do
  @external_resource Path.expand("./proto/vernemq_plugin", __DIR__)
  use Protobuf, from: Path.wildcard Path.expand("./proto/vernemq_plugin/*", __DIR__)

  use Alpgrow.RPC.Protocol, amqp_queue: "vernemq_plugin"
end

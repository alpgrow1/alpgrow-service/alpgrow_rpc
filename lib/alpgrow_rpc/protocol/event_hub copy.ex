defmodule Alpgrow.RPC.Protocol.DataUpdater do
  @external_resource Path.expand("./proto/data_updater", __DIR__)
    use Alpgrow.RPC.Protocol, amqp_queue: "data_updater"
end


defmodule Alpgrow.RPC.Protocol do

  defmacro __using__(opts) do
    case Keyword.fetch(opts, :amqp_queue) do
      :error -> IO.warn "AMQP que not passed to #{__MODULE__}", Macro.Env.stacktrace(__ENV__)
      {:ok, val} -> inject_amqp_queue(val)
    end
  end

  defp inject_amqp_queue(queue_name) do
    quote do
      def amqp_queue do
        unquote(queue_name)
      end
    end
  end
end

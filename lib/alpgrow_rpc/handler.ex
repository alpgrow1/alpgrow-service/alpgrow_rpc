defmodule Alpgrow.RPC.Handler do
  @moduledoc """
  Is a callback wrapper for RPC calls. That allows us a unique
  Interface for processing RPC calls.

  [Buissness Logic] <══════ [ Alpgrow.RPC (Lib)] <════ [AMQP Queue]\n
  ╚═════> [HANDLER] ══════> [ Alpgrow.RPC (Lib)] ════> [AMQP Queue]


  """

  @doc """
    Handles an RPC request
  """
  @callback handle_rpc(payload :: binary) ::
              :ok
              | {:ok, reply :: term}
              | {:error, :retry}
              | {:error, reason :: term}
end

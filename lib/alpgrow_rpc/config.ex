defmodule Alpgrow.RPC.Config do
  use Skogsra

  @envdoc "Username for accessing the AMQP broker."
  app_env(:amqp_connection_username, :alpgrow_rpc, :amqp_connection_username,
    os_env: "RPC_AMQP_CONNECTION_USERNAME",
    type: :binary,
    default: "alpgrow"
  )

  @envdoc "Password for accessing the AMQP broker."
  app_env(:amqp_connection_password, :alpgrow_rpc, :amqp_connection_password,
    os_env: "RPC_AMQP_CONNECTION_PASSWORD",
    type: :binary,
    default: "test123"
  )

  @envdoc "The hostname or IP of the AMQP broker."
  app_env(:amqp_connection_host, :alpgrow_rpc, :amqp_connection_host,
    os_env: "RPC_AMQP_CONNECTION_HOST",
    type: :binary,
    default: "127.0.0.1"
  )

  @envdoc "The Virtual Host to be used in the AMQP broker. Must be the same for all components."
  app_env(:amqp_connection_virtual_host, :alpgrow_rpc, :amqp_connection_virtual_host,
    os_env: "RPC_AMQP_CONNECTION_VIRTUAL_HOST",
    type: :binary,
    default: "/"
  )

  @envdoc "The port of the AMQP broker to connect to."
  app_env(:amqp_connection_port, :alpgrow_rpc, :amqp_connection_port,
    os_env: "RPC_AMQP_CONNECTION_PORT",
    type: :integer,
    default: 5672
  )

  @envdoc "The prefetch count of the AMQP connection. A prefetch count of 0 means unlimited (not recommended)."
  app_env(:amqp_prefetch_count, :alpgrow_rpc, :amqp_prefetch_count,
    os_env: "RPC_AMQP_PREFETCH_COUNT",
    type: :integer,
    default: 300,
    required: true
  )

  @envdoc """
  Max length of the server AMQP queue. If 0 the queue will be unbounded, otherwise it will be limited to tha    t length and new publishes will be dropped while the queue is full. WARNING: changing this value requires manually     deleting the queue
  """
  app_env(:amqp_queue_max_length, :alpgrow_rpc, :amqp_queue_max_length,
    os_env: "RPC_AMQP_QUEUE_MAX_LENGTH",
    type: :integer,
    default: 0
  )

  @envdoc "Enable SSL. If not specified, SSL is disabled."
  app_env(:amqp_connection_ssl_enabled, :alpgrow_rpc, :amqp_connection_ssl_enabled,
    os_env: "RPC_AMQP_CONNECTION_SSL_ENABLED",
    type: :boolean,
    default: false
  )

  @envdoc "Disable Server Name Indication. Defaults to false."
  app_env(
    :amqp_connection_ssl_disable_sni,
    :alpgrow_rpc,
    :amqp_connection_ssl_disable_sni,
    os_env: "RPC_AMQP_CONNECTION_SSL_DISABLE_SNI",
    type: :boolean,
    default: false
  )

  @envdoc "Specify the hostname to be used in TLS Server Name Indication extension. If not specified, the amqp host will be used. This value is used only if Server Name Indication is enabled."
  app_env(
    :amqp_connection_ssl_custom_sni,
    :alpgrow_appengine_api,
    :amqp_connection_ssl_custom_sni,
    os_env: "RPC_AMQP_CONNECTION_SSL_CUSTOM_SNI",
    type: :binary
  )

  @envdoc "Specifies the certificates of the root Certificate Authorities to be trusted. When not specified, the bundled cURL certificate bundle will be used."
  app_env(:amqp_connection_ssl_ca_file, :alpgrow_rpc, :amqp_connection_ssl_ca_file,
    os_env: "RPC_AMQP_CONNECTION_SSL_CA_FILE",
    type: :binary
  )

  @doc "The AMQP queue arguments."
  @type argument :: {:"x-max-length", integer()} | {:"x-overflow", String.t()}
  @spec amqp_queue_arguments!() :: [argument]
  def amqp_queue_arguments! do
    value = amqp_queue_max_length!()

    if value > 0 do
      ["x-max-length": value, "x-overflow": "reject-publish"]
    else
      []
    end
  end

  def amqp_options! do
    username = amqp_connection_username!()
    password = amqp_connection_password!()
    virtual_host = amqp_connection_virtual_host!()
    host = amqp_connection_host!()
    port = amqp_connection_port!()

    [
      username: username,
      password: password,
      virtual_host: virtual_host,
      host: host,
      port: port
    ]
  end
end
